#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "bmp.h"
#include "bmp_struct.h"

void print_error(error status);

int main(int arg_num, char** args)
{


	char* src_file = args[1];
    char* dst_file = args[2];
    double degrees = 90;
    if (arg_num >= 4) {
        char* degrees_str = args[3];
        degrees = strtod(degrees_str, NULL);
    }

  
    image* image;
    error status = image_read_from_bmp(src_file, &image);

    if (status == OK) {
        
        printf("Поворачиваем на %.2f граудсов.\n", degrees);
        rotate(image, degrees);
        error save_status = image_save_to_bmp(dst_file, image);

        if (save_status == OK) {
            puts("Готово!");
        } else { 
            print_error(save_status); 
        }
    } else { 
        print_error(status); 
    }

    return 0;
}

void print_error(error status) {
    if (status == FILE_ERR)
        puts("File error");
    else if (status == NOT_BMP_ERR)
        puts("wrong bmp format");
    else
        puts("Noooo");
}
