#include "bmp.h"
#include "bmp_struct.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>


error image_read_from_bmp(char* bmp_file_name, image** dst) {
    
    FILE* file = fopen(bmp_file_name, "rb");

    if (file == NULL) {
        return FILE_ERR; 
    }

    struct bmp_header header;
    fread(&header, sizeof(header), 1, file);

    if (header.type != MAGIC)
        return NOT_BMP_ERR;


    uint32_t pixel_data_start = header.data_start_addr;
    uint32_t width_bytes = header.width*3;
    uint32_t width_bytes_padding = 4 - width_bytes%4;

    fseek(file, pixel_data_start, SEEK_SET);

    image* result = image_create(header.width, header.height);

    struct bmp_pixel_bgr* bmp_pixel_data = (struct bmp_pixel_bgr*)malloc(sizeof(struct bmp_pixel_bgr) * width_bytes);

    for (uint32_t pixel_row_index = 0; pixel_row_index < header.height; pixel_row_index++) {
        
        fread(bmp_pixel_data, width_bytes, 1, file);
        for (uint32_t j = 0; j < image_get_width(result); j++) {
            
            struct bmp_pixel_bgr bmp_specific_pixel = bmp_pixel_data[j];

            pixel generic_pixel = (pixel) {
                bmp_pixel_data[j].r,
                bmp_pixel_data[j].g,
                bmp_pixel_data[j].b,
                255
            };

            point dest_point = (point) {
                j,
                image_get_height(result) - pixel_row_index - 1
            };
            image_set_pixel(result, dest_point, generic_pixel);
        }

        fseek(file, width_bytes_padding, SEEK_CUR);
    }


    free(bmp_pixel_data);
    fclose(file);

    *dst = result;

    return OK;
}

error image_save_to_bmp(char* bmp_file_name, image* src) {
    
    FILE* file = fopen(bmp_file_name, "wb");

    if (file == NULL) {
        
        return FILE_ERR; 
    }

    uint32_t width_bytes = image_get_width(src)*3;
    uint32_t width_bytes_padded = width_bytes % 4 == 0 ? width_bytes : width_bytes - width_bytes%4 + 4;

    struct bmp_header header;

    uint32_t file_size = sizeof(header) + width_bytes_padded * image_get_height(src);

    header.type = MAGIC; 
    header.file_size = file_size; 
    header.reserved = 0;    

    header.data_start_addr = sizeof(header);
    header.header_size = 40;    
    header.width = image_get_width(src);    
    header.height = image_get_height(src);  
    header.color_planes_num = 1;    
    header.bits_per_pixel = 24;     
    header.compression_method = 0;  
    header.image_size = 0;          
    header.x_pixels_per_meter = 1;  
    header.y_pixels_per_meter = 1;  
    header.colors_num = 256*256*256;
    header.important_colors_num = 0;


    fwrite(&header, sizeof(header), 1, file);

    struct bmp_pixel_bgr* pixel_data = (struct bmp_pixel_bgr*)malloc(width_bytes_padded);
    
    for (uint32_t row = 0; row < image_get_height(src); row++) {
        
        for (uint32_t pixelIdx = 0; pixelIdx < image_get_width(src); pixelIdx++) {
           
            pixel px = image_get_pixel(src, (point) { pixelIdx, image_get_height(src) - row - 1 });
        
            pixel_data[pixelIdx] = (struct bmp_pixel_bgr) {
                px.b, px.g, px.r
            };
        }

        fwrite(pixel_data, width_bytes_padded, 1, file);

        if (ferror(file))
            return FILE_ERR; 
    }

    free(pixel_data);
    fclose(file);

    return OK;
}

void rotate(image *image, double degrees)
{
    double radians = degrees*M_PI/180;

    bool size_swap_needed = fmod(fabs(degrees) + 45, 180) > 90;

    uint32_t width = image_get_width(image);
    uint32_t height = image_get_height(image);
    struct image* old_image = image_copy(image);

    double center_x = (double)width/2.0;
    double center_y = (double)height/2.0;

    double cosr = cos(-radians);
    double sinr = sin(-radians);

    double transform_matrix3[] = {
        cos(-radians), -sin(-radians), - center_x*cos(-radians) + center_y*sin(-radians) + center_x,
        sin(-radians),  cos(-radians), - center_x*sin(-radians) - center_y*cos(-radians) + center_y,
        0, 0, 1
    };

    if (size_swap_needed) {

        uint32_t buffer = image->width;
        image->width = image->height;
        image->height = buffer;
        
        width = image_get_width(image);
        height = image_get_height(image);

        transform_matrix3[2] = - center_y*cos(-radians) + center_x*sin(-radians) + center_x;
        transform_matrix3[5] = - center_y*sin(-radians) - center_x*cos(-radians) + center_y;
    }

    
    for (uint32_t current_y = 0; current_y < height; current_y++) {
       
        for (uint32_t current_x = 0; current_x < width; current_x++) {
           
            point dst_point = { current_x, current_y };

            double src_x = dst_point.x;
            double src_y = dst_point.y;
            src_x += 0.5;
            src_y += 0.5;
            point src_point = {
                floor(transform_matrix3[0]*src_x + transform_matrix3[1]*src_y + transform_matrix3[2]),
                floor(transform_matrix3[3]*src_x + transform_matrix3[4]*src_y + transform_matrix3[5])
            };
            pixel src_pixel = image_get_pixel(old_image, src_point);
            image_set_pixel(image, dst_point, src_pixel);
        }
    }
    
    free(old_image->data_array);
    free(old_image);


}


image* image_create(uint32_t width, uint32_t height) {
    
    image* new_image = malloc(sizeof(image));
    new_image->width = width;
    new_image->height = height;
    size_t pixel_count = width * height;
    size_t data_array_size = sizeof(pixel) * pixel_count;
    pixel* new_data_array = malloc(data_array_size);

    for (size_t i = 0; i < pixel_count; i++)
        new_data_array[i] = (pixel) {0, 0, 0, 0};

    new_image->data_array = new_data_array;
    return new_image;
}

image* image_copy(image* image) {

    uint32_t width = image_get_width(image);
    uint32_t height = image_get_height(image);
   
    size_t pixel_count = width * height;
    size_t image_data_size = sizeof(pixel) * width * height;
    struct image* new_image = image_create(width, height);
    pixel* new_image_data = new_image->data_array;
    pixel* source_image_data = image->data_array;
    memcpy(new_image_data, source_image_data, image_data_size);
    return new_image;
}


uint32_t image_get_width(image* image) {
    return image->width;
}

uint32_t image_get_height(image* image) {
    return image->height;
}

pixel image_get_pixel(image* image, point point) {
    if (image == NULL)
        return (pixel) {0, 0, 0, 0}; 
    
    
    if (!image_check_bounds(image, point))
        return (pixel) {0, 0, 0, 0}; 
    
    size_t pixel_index = image_get_width(image) * point.y + point.x;

    return image->data_array[pixel_index];
}


bool image_set_pixel(image* image, point point, pixel pixel) {

    if (image == NULL)
        return false; 
  
    if (!image_check_bounds(image, point))
        return false; 
    
    size_t pixel_index = image_get_width(image) * point.y + point.x;
    image->data_array[pixel_index] = pixel;

    return true;
}

bool image_check_bounds(image* image, point point) {
    
    if (image == NULL) {
        return false;
    }

    return !(
        point.x < 0 ||
        point.x >= image_get_width(image) ||
        point.y < 0 ||
        point.y >= image_get_height(image)
    );
}



