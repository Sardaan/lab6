all: bmp.c main.c
	gcc -o main bmp.c main.c

clean:
	rm main *.o

run1: main
	./main tests/1.bmp tests/1_r.bmp

run2: main
	./main tests/2.bmp tests/2_r.bmp

run3: main
	./main tests/2.bmp tests/3_r.bmp 30

runF: main
	./main tests/image1.bmp tests/image1_r.bmp
